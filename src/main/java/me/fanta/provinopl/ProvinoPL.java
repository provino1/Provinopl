package me.fanta.provinopl;

import me.fanta.provinopl.commands.KitCommand;
import me.fanta.provinopl.commands.SetSpawnCommand;
import me.fanta.provinopl.commands.SpawnCommand;
import me.fanta.provinopl.events.PlayerJoinQuitEvents;
import me.fanta.provinopl.manager.ConfigManager;
import me.fanta.provinopl.manager.CoolDownManager;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public final class ProvinoPL extends JavaPlugin {
     
    private File fileData = new File(this.getDataFolder() + File.separator +"data.yml");
    private YamlConfiguration yamlFileData;
    public static ProvinoPL main;


    @Override
    public void onEnable() {
        this.main = this;

        this.getConfig().options().copyDefaults(true);
        this.saveDefaultConfig();

        initializingFiles();

        this.getServer().getPluginManager().registerEvents(new PlayerJoinQuitEvents(), this);
        this.getCommand("setspawn").setExecutor(new SetSpawnCommand());
        this.getCommand("spawn").setExecutor(new SpawnCommand());
        this.getCommand("kit").setExecutor(new KitCommand());


    }

    @Override
    public void onDisable() {
        try {
            CoolDownManager.saveCoolDownToFile(yamlFileData, fileData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initializingFiles(){
        try{
            if(!fileData.exists()){
                fileData.createNewFile();
                yamlFileData = new YamlConfiguration();
                yamlFileData.load(fileData);

            } else {
                yamlFileData = new YamlConfiguration();
                yamlFileData.load(fileData);

                CoolDownManager.loadCoolDownToFile(yamlFileData, fileData);
            }

        }catch(NullPointerException e){
            System.out.println("Recupero cooldown kit fallito, nessun dato precedente trovato");
        }
        catch(Exception e){
            e.printStackTrace();
        }


    }

}
