package me.fanta.provinopl.manager;

import me.fanta.provinopl.ProvinoPL;

import me.fanta.provinopl.utils.KitConverter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class ConfigManager {

    public static String messageJoin = getStringFromConfig("joinquit.join.message");
    public static String soundJoinName = getStringFromConfig("joinquit.join.sound");
    public static String effectJoinName = getStringFromConfig("joinquit.join.effect");

    public static String messageQuit = getStringFromConfig("joinquit.quit.message");
    public static String soundQuitName = getStringFromConfig("joinquit.quit.sound");
    public static String effectQuitName = getStringFromConfig("joinquit.quit.effect");

    public static String messageTeleport = getStringFromConfig("messages.messageTeleport");
    public static String messageSetSpawn = getStringFromConfig("messages.messageSetSpawn");
    public static String messageKitTaken = getStringFromConfig("messages.messageKitTaken");


    public static String locWorld(){
        return getStringFromConfig("spawn.location.world");
    }
    public static String locX(){ return getStringFromConfig("spawn.location.x");}
    public static String locY() {  return getStringFromConfig("spawn.location.y");}
    public static String locZ() {  return getStringFromConfig("spawn.location.z");}
    public static String locPitch() { return getStringFromConfig("spawn.location.pitch");}
    public static String locYaw() { return getStringFromConfig("spawn.location.yaw");}



    private static String getStringFromConfig(String path) {

        return ProvinoPL.main.getConfig().getString(path);
    }

    private static List<String> getListStringFromConfig(String path) {
        return ProvinoPL.main.getConfig().getStringList(path);
    }

    private static void setStringFromConfig(String path, String value) {
        ProvinoPL.main.getConfig().set(path, value);
    }

    /*
    spawn:
    location:
        world: "world"
        x: "1"
        y: "2"
        z: "3"
        pitch: "4"
        yaw: "6"

     */

    public static void setLocationToConfig(Location spawn) {
        setStringFromConfig("spawn.location.world", spawn.getWorld().getName());
        setStringFromConfig("spawn.location.x", String.valueOf(spawn.getX()));
        setStringFromConfig("spawn.location.y", String.valueOf(spawn.getY()));
        setStringFromConfig("spawn.location.z", String.valueOf(spawn.getZ()));
        setStringFromConfig("spawn.location.yaw", String.valueOf(spawn.getYaw()));
        setStringFromConfig("spawn.location.pitch", String.valueOf(spawn.getPitch()));
        ProvinoPL.main.saveConfig();
        ProvinoPL.main.reloadConfig();
    }


    public static String getKits(Player p, String kitName) {
        int slotLiberi = 0;
        List<String> kitItems = getListStringFromConfig("kits." + kitName + ".items");
        if (kitItems != null || kitItems.size() == 0) {
            List<ItemStack> items = KitConverter.getKitFromStrings(kitItems);
            if(items.size() == 0){
                return "Errore inaspettato";
            }

            for(ItemStack item : p.getInventory().getContents()){
                if(item.getType() == Material.AIR){
                    slotLiberi++;
                }
            }

            if(slotLiberi < kitItems.size()){
                return "Non hai abbastanza slot liberi";
            }

            for(ItemStack item : items){
                p.getInventory().addItem(item);
            }

            return "Ti ho dato il kit con successo";
        } else {
            return "Questo kit non esiste";
        }
    }

    public static Long getKitCooldown(String kitName){
        if(ProvinoPL.main.getConfig().getString("kits."+kitName+".cooldown") != null){
            return ProvinoPL.main.getConfig().getLong("kits."+kitName+".cooldown");

        }else {
            return null;
        }
    }


    public static String getKitNames(){
        StringBuilder sb = new StringBuilder("");
        for(String kitName : ProvinoPL.main.getConfig().getConfigurationSection("kits").getKeys(false)){
            sb.append(kitName + ",");
        }
        return sb.toString();
    }

}
