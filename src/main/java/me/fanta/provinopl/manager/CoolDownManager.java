package me.fanta.provinopl.manager;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CoolDownManager {

    private static HashMap<UUID, HashMap<String, Long>> coolDown = new HashMap<>();

    public static void addPlayerToCooldown(Player p, String nameKit) {
        HashMap<String, Long> internalHashMap;
        if(coolDown.containsKey(p.getUniqueId())){
            internalHashMap = coolDown.get(p.getUniqueId());

        } else {
            internalHashMap = new HashMap<>();
        }
        internalHashMap.put(nameKit, System.currentTimeMillis());

        coolDown.put(p.getUniqueId(), internalHashMap);
    }


    public static boolean isCoolDownPassed(Player p, String nameKit, Long cooldownFromConfig) {
        if(coolDown.containsKey(p.getUniqueId())){
            long difference = System.currentTimeMillis() - cooldownFromConfig;
            if(difference - cooldownFromConfig <= 0){
                coolDown.get(p.getUniqueId()).put(nameKit, null);
                return true;
            }
        } else {
            return true;
        }
        return false;
    }


    public static void saveCoolDownToFile(YamlConfiguration yaml, File file) throws IOException {
        for(UUID key : coolDown.keySet()){
            yaml.set(key.toString(), "");
            for(String kitName : coolDown.get(key).keySet()){
                yaml.set(key+"."+kitName, coolDown.get(key).get(kitName));
            }
        }
        yaml.save(file);
    }


    public static void loadCoolDownToFile(YamlConfiguration yaml, File file) throws IOException {
        for(String key : yaml.getKeys(false)){
            HashMap<String, Long> nuovoHash = new HashMap<>();
            for(String kitName : yaml.getConfigurationSection("key").getKeys(false)){
                nuovoHash.put(kitName, yaml.getLong(key+"."+kitName));
            }
            coolDown.put(UUID.fromString(key), nuovoHash);
        }
    }



}
