package me.fanta.provinopl.commands;

import me.fanta.provinopl.manager.ConfigManager;
import me.fanta.provinopl.manager.CoolDownManager;
import me.fanta.provinopl.utils.KitConverter;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class KitCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            Player p = (Player) sender;
            if(args.length == 1){
                if(ConfigManager.getKitCooldown(args[0]) != null){
                    if(CoolDownManager.isCoolDownPassed(p, args[0], ConfigManager.getKitCooldown(args[0]))){
                        p.sendMessage(ConfigManager.getKits(p, args[0]));
                        CoolDownManager.addPlayerToCooldown(p, args[0]);
                        return true;
                    }else {
                        p.sendMessage("Non puoi ancora usare questo kit, c'è un cooldown in corso");
                        return false;
                    }
                }

            } else {
                p.sendMessage(ConfigManager.getKitNames());
            }
         }
        return false;
    }
}
