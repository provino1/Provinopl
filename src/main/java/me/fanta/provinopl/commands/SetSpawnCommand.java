package me.fanta.provinopl.commands;

import me.fanta.provinopl.manager.ConfigManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;

public class SetSpawnCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            Player p = (Player) sender;
            p.sendMessage(ConfigManager.messageSetSpawn);
            ConfigManager.setLocationToConfig(p.getLocation());
            return true;
        }


        return false;
    }
}
