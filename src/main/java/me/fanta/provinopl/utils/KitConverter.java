package me.fanta.provinopl.utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class KitConverter {


/*
kits:
    swordsman:
        - DIAMOND_SWORD:1:SHARPNESS;1,UNBREAKING;1:Spadona
 */

    public static List<ItemStack> getKitFromStrings(List<String> stringToConvert){
        List<ItemStack> items = new ArrayList<>();
        for(String kitItem : stringToConvert){
            try {
                String[] kitItems = kitItem.split(":");
                ItemStack generatingItem = new ItemStack(Material.valueOf(kitItems[0]));
                generatingItem.setAmount(Integer.valueOf(kitItems[1]));
                String[] enchants = kitItems[2].split(",");
                for(String enchant : enchants){
                    String[] enchantAndLevel = enchant.split(";");
                    System.out.println(enchantAndLevel[0]);
                    generatingItem.addEnchantment(Enchantment.getByKey(NameSpacedKey.minecraft(enchantAndLevel[0]), Integer.valueOf(enchantAndLevel[1]));
                }
                ItemMeta generatingItemMeta = generatingItem.getItemMeta();
                generatingItemMeta.setDisplayName(kitItems[3]);
                generatingItem.setItemMeta(generatingItemMeta);
                items.add(generatingItem);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return items;
    }

}
