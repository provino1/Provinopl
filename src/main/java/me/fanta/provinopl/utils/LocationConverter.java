package me.fanta.provinopl.utils;

import me.fanta.provinopl.ProvinoPL;
import me.fanta.provinopl.manager.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationConverter {

    public static Location getLocationFromConfig(){
        return new Location(Bukkit.getWorld(ConfigManager.locWorld()), Double.valueOf(ConfigManager.locX()), Double.valueOf(ConfigManager.locY()),
                Double.valueOf(ConfigManager.locZ()), Float.valueOf(ConfigManager.locYaw()), Float.valueOf(ConfigManager.locPitch()));
    }



}
