package me.fanta.provinopl.events;

import me.fanta.provinopl.manager.ConfigManager;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerJoinQuitEvents implements Listener {

    @EventHandler
    public void joinEvent(PlayerJoinEvent e){
        Player p = e.getPlayer();
        e.setJoinMessage(ConfigManager.messageJoin.replace("{player}", p.getName()));
        p.playSound(p.getLocation(), Sound.valueOf(ConfigManager.soundJoinName), 1, 1);
        p.playEffect(p.getLocation(), Effect.valueOf(ConfigManager.effectJoinName), "");

    }

    @EventHandler
    public void quitEvent(PlayerQuitEvent e){
        Player p = e.getPlayer();
        e.setQuitMessage(ConfigManager.messageQuit.replace("{player}", p.getName()));
        p.playSound(p.getLocation(), Sound.valueOf(ConfigManager.soundQuitName), 1, 1);
        p.playEffect(p.getLocation(), Effect.valueOf(ConfigManager.effectQuitName), "");

    }

}
